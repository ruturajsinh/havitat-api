const express = require('express');
const HavitatRoutes = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require('../config/keys');

// load validation
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');

// Load model
const User = require('../model/User');
const passport = require('passport');

// user register route
HavitatRoutes.post('/register', (req,res) => {
    const {errors,isValid} = validateRegisterInput(req.body);

    // check validation
    if(!isValid){
        return res.status(400).json(errors);
    }

    User.findOne({email:req.body.email}).then(user => {
        if(user){
            return res.status(400).json({email:"This is email address already with us"});
        }else{
            const newUser = new User({
                name:req.body.name,
                email:req.body.email,
                password: req.body.password,
                number: req.body.number,
                signup_by: "normal"
            });
            
            // hash password before saving in database
            bcrypt.genSalt(10,(err,salt) => {
                bcrypt.hash(newUser.password, salt, (err,hash)=>{
                    if(err) throw err;
                    newUser.password = hash;
                    newUser.save()
                    .then(user => res.status(200).json(user))
                    .catch(err => console.log(err));
                })
            });
        }
    })
});

HavitatRoutes.post('/register-with-otp', (req,res) => {
    const {errors,isValid} = validateRegisterInput(req.body);

    // check validation
    if(!isValid){
        return res.status(400).json(errors);
    }

    User.findOne({email:req.body.number}).then(user => {
        if(user){
            return res.status(400).json({email:"This is email address already with us"});
        }else{
            const newUser = new User({
                name:req.body.name,
                email:req.body.email,
                password: req.body.password,
                number: req.body.number,
                signup_by: "mobile"
            });
            
            // hash password before saving in database
            bcrypt.genSalt(10,(err,salt) => {
                bcrypt.hash(newUser.password, salt, (err,hash)=>{
                    if(err) throw err;
                    newUser.password = hash;
                    newUser.save()
                    .then(user =>{
                        if(user){
                            const payload = {
                                id: user.id,
                                name: user.name
                            }
            
                            // Sign token
                            jwt.sign(
                                payload,
                                keys.secretOrKey,
                                {
                                    expiresIn: 31556926 // 1 year in second
                                },
                                (err, token) => {
                                    res.json({
                                        success: true,
                                        token: "Bearer " + token
                                    });
                                }
                            );
                        }else{
                            res.json({ success: false, message: "Something went wrong while create register" });
                        }
                    })
                    .catch(err => console.log(err));
                })
            });
        }
    })
});

HavitatRoutes.post("/login", (req,res) => {
    
    const {errors,isValid} = validateLoginInput(req.body);

    if(!isValid){
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    User.findOne({email}).then(user => {
        if(!user){
            return res.status(404).json({emailnotfound:"Email address not found"});
        }

        // check password
        bcrypt.compare(password,user.password).then(isMatch => {
            if(isMatch){
                // create jwt payload
                const payload = {
                    id: user.id,
                    name: user.name
                }

                // Sign token
                jwt.sign(
                    payload,
                    keys.secretOrKey,
                    {
                        expiresIn: 31556926 // 1 year in second
                    },
                    (err, token) => {
                        res.json({
                            success: true,
                            token: "Bearer " + token
                        });
                    }
                );
            }else{
                return res.status(400).json({ passwordincorrect: "password incorrect" });
            }
        });
    });

});

HavitatRoutes.post('/login-with-otp', (req,res) => {

    User.findOne({number: req.number}).then(user => {
        if(user){
            const payload = {
                id: user.id,
                name: user.name
            }

            // Sign token
            jwt.sign(
                payload,
                keys.secretOrKey,
                {
                    expiresIn: 31556926 // 1 year in second
                },
                (err, token) => {
                    res.json({
                        success: true,
                        token: "Bearer " + token
                    });
                }
            );
        }else{
            res.status(400).json({ "status":"error","message":"something went wrong while trying to login, Please try again." });
        }
    }).catch(err => { console.log(err); });

});

HavitatRoutes.get('/auth/google', passport.authenticate('google',{ scope:['profile','email'] }));
HavitatRoutes.get('/auth/google/callback', passport.authenticate('google',
{ failureRedirect:'/', session: false }), (req,res) => {
    let token = jwt.sign({
        data: req.user
        }, 'secret', { expiresIn: 31556926 }); // expiry in seconds
    
    var newToken = "Bearer " + token;
    res.cookie("jwtToken",newToken);

    res.redirect('http://localhost:3000/create-property');
});


HavitatRoutes.get('/auth/facebook', passport.authenticate('facebook', {scope:'email'}));
HavitatRoutes.get('/auth/facebook/callback', passport.authenticate('facebook',
{ failureRedirect:'/', session: false }), (req,res) => {
    let token = jwt.sign({
        data: req.user
        }, 'secret', { expiresIn: 31556926 }); // expiry in seconds
    
    var newToken = "Bearer " + token;

    res.cookie("jwtToken",newToken);
    
    res.redirect('http://localhost:3000/create-property');
});


HavitatRoutes.get('/check-user-exits', (req,res) => {

    User.findOne({number: req.query.number}).then(user => {
        if(user){
            res.json({ success: true, data: user })
        }else{
            res.json({ success: false, message: "User Not found" });
        }
    }).catch(err => { console.log(err); });

});

module.exports = HavitatRoutes;

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}