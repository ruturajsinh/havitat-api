const Validator = require('validator');
const isEmpty = require('is-empty');

module.exports = function validateRegisterInput(data) {
    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    data.password = !isEmpty(data.password) ? data.password : "";
    data.c_password = !isEmpty(data.c_password) ? data.c_password : "";
    data.number = !isEmpty(data.number) ? data.number : "";

    if (Validator.isEmpty(data.name)) {
        errors.name = "name field is required";
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = "email field is required";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "email is invalid";
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = "password field is reqired";
    }
    if (Validator.isEmpty(data.c_password)) {
        errors.c_password = "please fill confirm password field";
    }

    if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
        errors.password = "password must be at least 6 characters";
    }

    if (!Validator.equals(data.password, data.c_password)) {
        errors.c_password = "password must match";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}