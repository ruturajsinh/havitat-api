const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const passport = require('passport');
const User = mongoose.model('users');
const keys = require('../config/keys');

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrKey;

var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

module.exports = passport => {

    // use to serialize the user for the session
    passport.serializeUser(function(user,done){
        done(null, user.id);
    });

    // deserialize the user 
    passport.deserializeUser(function(id,done){
        User.findById(id, function(err,user){
            done(err,user);
        });
    });

    passport.use(
        new JwtStrategy(opts, (jwt_payload, done) => {
            User.findById(jwt_payload.id)
            .then(user => {
                if(user){
                    return done(null, user);
                }
                return done(null, false);
            }).catch(err => console.log(err));
        })
    );

    passport.use(new GoogleStrategy({
            clientID        : keys.googleAuth.clientID,
            clientSecret    : keys.googleAuth.clientSecret,
            callbackURL     : keys.googleAuth.callbackURL
        },
    
        function(token, refreshToken, profile, done){
            process.nextTick(function(){
                User.findOne({ 'google_id': profile.id }, function(err,user){
                    if(err)
                        return done(err);
                    
                    if(user){
                        return done(null, user);
                    }else{
                        var newUser = new User();
                        console.log(profile);
                        newUser.google_id = profile.id;
                        newUser.token = token;
                        newUser.name =  profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.email = profile.emails[0].value;
                        newUser.signup_by = "google";
                        newUser.save(function(err){
                            if(err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });
            });
        }
    ));


    passport.use(new FacebookStrategy({
        clientID        : keys.facebookAuth.clientID,
        clientSecret    : keys.facebookAuth.clientSecret,
        callbackURL     : keys.facebookAuth.callbackURL
    },

    function(token, refreshToken, profile, done){
        process.nextTick(function(){
            User.findOne({ 'facebook_id': profile.id }, function(err,user){
                if(err)
                    return done(err);
                
                if(user){
                    return done(null, user);
                }else{
                    var newUser = new User();
                    newUser.facebook_id = profile.id;
                    newUser.token = token;
                    newUser.name =  profile.displayName;
                    newUser.email = profile.email;
                    newUser.signup_by = "facebook";
                    newUser.save(function(err){
                        if(err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });
    }
));


};